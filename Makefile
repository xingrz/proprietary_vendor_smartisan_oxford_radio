DEVICE := oxford

MODEM_IMAGE := firmware-update/NON-HLOS.bin
XBL_ELF := firmware-update/xbl.elf
ABL_ELF := firmware-update/abl.elf

TIMESTAMP := $(shell strings $(MODEM_IMAGE) | sed -n 's/.*"Time_Stamp": "\([^"]*\)"/\1/p')
VERSION := $(shell echo $(TIMESTAMP) | sed 's/[ :-]*//g')

HASH_XBL := $(shell openssl dgst -r -sha1 $(XBL_ELF) | cut -d ' ' -f 1)
HASH_ABL := $(shell openssl dgst -r -sha1 $(ABL_ELF) | cut -d ' ' -f 1)

TARGET := RADIO-$(DEVICE)-$(VERSION).zip

# Build
# ==========

.PHONY: build
build: assert inspect $(TARGET)
	@echo Size: $(shell stat -f %z $(TARGET))

$(TARGET): META-INF firmware-update
	zip -r9 $@ $^

# Clean
# ==========

.PHONY: clean
clean:
	rm -f *.zip

# Assert
# ==========
.PHONY: assert
assert: $(XBL_ELF) $(ABL_ELF)
ifneq ($(HASH_XBL), c64b75c29b4db9d3ed1723e5e1b88ec6efabb4ae)
	$(error SHA-1 of xbl.elf mismatch)
endif
ifneq ($(HASH_ABL), ae0a61156e056c5ba26a4327d3d84ea585684c8e)
	$(error SHA-1 of abl.elf mismatch)
endif

# Inspect
# ==========

.PHONY: inspect
inspect: $(MODEM_IMAGE)
	@echo Target: $(TARGET)
	@echo Timestamp: $(TIMESTAMP)
